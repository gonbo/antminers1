#!/bin/sh
cd /home/gonbo/antminer
ips=$(netstat -tnp| grep 200:3334 | awk '{split($5,a,":");print a[1]}')
for ip in $ips
do
ssh-keygen -f "/home/gonbo/.ssh/known_hosts" -R $ip
./restartcgminer.expect $ip root root
done
