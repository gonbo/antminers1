#/bin/sh
cp -rf ./asic-freq /etc/config/
cp -rf ./cgminer /etc/config/
cp -rf ./updateconf.sh /etc/
cp -rf ./monitor_agent /etc/
cp -rf ./crontab /etc/crontabs/root
cp -rf ./installwget.sh /etc/
MAC=$(ifconfig eth1| grep HWaddr | awk -F" " '{n=split($5,a,":");print a[1] a[2] a[3] a[4] a[5] a[6]}' )
user=$(uci get cgminer.default.pool1user)
if [ $user != "cexsxant.$MAC" ]
then
uci set cgminer.default.pool1user="cexsxant.$MAC"
uci set cgminer.default.pool2user="cexsxant.$MAC"
uci set cgminer.default.pool3user="cexsxant.$MAC"
uci commit cgminer
fi

