#!/bin/sh

IP=`ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}' | tail -1`
RANDOM=$(echo $IP  | awk -F"." '{print $4}')
RANDOM=`expr $RANDOM % 20`
sleep $RANDOM 


cd /tmp/
rm antconfig.tar.gz
busybox wget http://10.0.255.200/antconfig.tar.gz 
md5sum -c antconfig.tar.gz.md5
if [[ $? -eq 0 ]]
then
exit 0
fi
md5sum antconfig.tar.gz > antconfig.tar.gz.md5
tar xzvf antconfig.tar.gz
cd antconfig
./confinstall.sh 
