#/bin/sh
cd /tmp/
if [ -f /etc/wgetdone ]
then
exit 0
fi
busybox wget http://10.0.255.200/libopenssl_1.0.1e-2_ar71xx.ipk
opkg install libopenssl_1.0.1e-2_ar71xx.ipk
busybox wget http://10.0.255.200/libpcre_8.11-2_ar71xx.ipk
opkg install libpcre_8.11-2_ar71xx.ipk
busybox wget http://10.0.255.200/wget_1.14-1_ar71xx.ipk
opkg install wget_1.14-1_ar71xx.ipk
touch /etc/wgetdone
echo "Welcome !"
