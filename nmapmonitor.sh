#!/bin/sh
cd /home/gonbo/juwabi-client
while [ 1 ]
do
maclist=$(cat ./maclist| awk '{print $1}')
ipnetlist=$(ip addr | grep eth | grep inet | awk '{print $2}')
echo $ipnetlist
for ip in $ipnetlist
do
nmap  -T5 --min-parallelism 1000 -sP  -n $ip > /tmp/inittmp.log
newmaclist=$(cat /tmp/inittmp.log | awk /MAC/'{print $3}')
echo $newmaclist
newiplist=$(cat /tmp/inittmp.log | awk /Nmap/'{print $5}')
echo $newiplist
done
sleep 10
done
